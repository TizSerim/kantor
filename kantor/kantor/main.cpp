#include <iostream>
#include <regex>
#include <fstream>
#include <string>
#include <list>
#include <algorithm>
#include <map>

using namespace std;

int main(int argc, char* argv[])
{
	fstream mydata("data.txt", ios::in);  

	if (!mydata.is_open()) {
		cout << "Niestety nie udalo sie otworzyc pliku.";
		return 1;
	}
	regex primalData("<title>1 USD = ([0-9]+\.?[0-9]*) ([A-Z]*)</title>");
	regex fullName("<description>1 U.S. Dollar = [0-9]+.[0-9]* ([[:alpha:],\ ,\.]*)");
	smatch match;

	string line;
	list <string> mylines;
	map <string, pair <string, double>> data;
	while (mydata.good()) {
		getline(mydata, line);
		mylines.push_front(line);
	}
	string shortname;
	double value;

	for (auto itr = mylines.rbegin(); itr != mylines.rend(); itr++) {

		if (regex_search(*itr, match, primalData))
		{
			shortname = match.str(2);
			value = stod(match.str(1));
		}
		if (regex_search(*itr, match, fullName))
		{
			data[shortname].first = match.str(1);
			data[shortname].second = value;
			//cout << shortname << " " << match.str(1) << " " << value<<endl;
		}
	}
	data["USD"].first="U.S. Dollar";
	data["USD"].second = 1;
	while (1) {
		string from, to;
		double amount;
		cout << "Which currency would u like to change e.g. EUR" << endl;
		cin >> from;
		cout << "And how much?" << endl;
		cin >> amount;
		cout << "To what currency?" << endl;
		cin >> to;
		transform(to.begin(), to.end(), to.begin(), ::toupper);
		transform(from.begin(), from.end(), from.begin(), ::toupper);
		
		cout << amount << " " << from << " equals " << data[to].second / data[from].second*amount << " " << to << endl;
	}
	cin.ignore();
	getchar();
	return 0;
}